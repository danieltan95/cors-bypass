(ns cors-bypass.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [clj-http.client :as client]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.json :refer [wrap-json-response]]
            [ring.util.response :refer [response]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]))

(defn get-url [url]
  (:body (client/get url {:as :auto})))

(defroutes app-routes
  (GET "/" []
       "Try a url")
  (GET ["/site/:url" :url #".+"] [url]
       (get-url url))
  (context
   "/json" []
   (GET ["/:url" :url #".+"] [url]
        (response (get-url url))))
  (route/not-found "Not Found"))

(def app
  (-> app-routes
      (wrap-cors)
      (wrap-json-response)
      (wrap-defaults site-defaults)))
