# cors-bypass

This webserver bypasses cors for you so you don't need to.

Use cases:
1. accessing an api that you have no control over.
2. development testing

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server
    
Then go to localhost:3000/json/<external-url-here> for api-based requests

## License

Copyright © 2020 FIXME
